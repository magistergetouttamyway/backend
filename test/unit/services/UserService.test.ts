import { User } from '../../../src/api/models/User';
import { ClientService } from '../../../src/api/services/ClientService';
import { events } from '../../../src/api/subscribers/events';
import { EventDispatcherMock } from '../lib/EventDispatcherMock';
import { LogMock } from '../lib/LogMock';
import { RepositoryMock } from '../lib/RepositoryMock';
import { Client } from '../../../src/api/models/Client';
import { LocationService } from '../../../src/api/services/LocationService';

describe('ClientService', () => {
    test('Find should return a list of clients', async done => {
        const log = new LogMock();
        const repo = new RepositoryMock();
        const locationRepo = new RepositoryMock();
        const locationService = new LocationService(locationRepo as any, log);
        const ed = new EventDispatcherMock();
        const user = new User();
        user.id = '1';
        user.email = 'john.doe@test.com';

        const client = new Client();
        client.firstName = 'John';
        client.lastName = 'Doe';
        client.id = '2';
        client.user = user;

        repo.list = [client];
        const clientService = new ClientService(locationService, repo as any, ed as any, log);
        const list = await clientService.find();
        expect(list[0].firstName).toBe(client.firstName);
        done();
    });

    test('Create should dispatch subscribers', async done => {
        const log = new LogMock();
        const repo = new RepositoryMock();
        const ed = new EventDispatcherMock();
        const locationRepo = new RepositoryMock();
        const locationService = new LocationService(locationRepo as any, log);
        const user = new User();
        user.id = '1';
        user.email = 'john.doe@test.com';

        const client = new Client();
        client.firstName = 'John';
        client.lastName = 'Doe';
        client.id = '2';
        client.user = user;

        const clientService = new ClientService(locationService, repo as any, ed as any, log);
        const newClient = await clientService.create(client);
        expect(ed.dispatchMock).toBeCalledWith([events.user.created, newClient]);
        done();
    });
});
