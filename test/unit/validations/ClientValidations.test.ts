import { validate } from 'class-validator';

import { Client } from '../../../src/api/models/Client';

describe('ClientValidations', () => {
    test('Client should always have a first name', async done => {
        const client = new Client();
        const errorsOne = await validate(client);
        client.firstName = 'TestName';
        const errorsTwo = await validate(client);
        expect(errorsOne.length).toBeGreaterThan(errorsTwo.length);
        done();
    });

    test('Client should always have a last name', async done => {
        const client = new Client();
        const errorsOne = await validate(client);
        client.lastName = 'TestName';
        const errorsTwo = await validate(client);
        expect(errorsOne.length).toBeGreaterThan(errorsTwo.length);
        done();
    });

    test('Client validation should succeed with all required fields', async done => {
        const client = new Client();
        client.firstName = 'test';
        client.lastName = 'lasttest';
        const errors = await validate(client);
        expect(errors.length).toEqual(0);
        done();
    });
});
