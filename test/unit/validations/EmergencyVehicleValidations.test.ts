import { validate } from 'class-validator';
import { Authority } from '../../../src/api/models/Authority';
import { EmergencyVehicleState, EmergencyVehicleType } from '../../../src/api/models/EmergencyVehicle';
import { EmergencyVehicle } from '../../../src/api/models/EmergencyVehicle';

describe('EmergencyVehicleValidations', () => {
    test('Emergency Vehicle should always have a name', async done => {
        const emergencyVehicle = new EmergencyVehicle();
        const errorsOne = await validate(emergencyVehicle);
        emergencyVehicle.name = 'test';
        const errorsTwo = await validate(emergencyVehicle);
        expect(errorsOne.length).toBeGreaterThan(errorsTwo.length);
        done();
    });

    test('Emergency Vehicle should always have a type', async done => {
        const emergencyVehicle = new EmergencyVehicle();
        const errorsOne = await validate(emergencyVehicle);
        emergencyVehicle.type = EmergencyVehicleType.Police;
        const errorsTwo = await validate(emergencyVehicle);
        expect(errorsOne.length).toBeGreaterThan(errorsTwo.length);
        done();
    });

    test('Emergency Vehicle should always have a authority', async done => {
        const emergencyVehicle = new EmergencyVehicle();
        const authority = new Authority();
        authority.name = 'TestAuthority';
        const errorsOne = await validate(emergencyVehicle);
        emergencyVehicle.authority = authority;
        const errorsTwo = await validate(emergencyVehicle);
        expect(errorsOne.length).toBeGreaterThan(errorsTwo.length);
        done();
    });

    test('Emergency Vehicle should always have a state', async done => {
        const emergencyVehicle = new EmergencyVehicle();
        const errorsOne = await validate(emergencyVehicle);
        emergencyVehicle.state = EmergencyVehicleState.Active;
        const errorsTwo = await validate(emergencyVehicle);
        expect(errorsOne.length).toBeGreaterThan(errorsTwo.length);
        done();
    });

    test('Emergency Vehicle validation should succeed with all required fields', async done => {
        const emergencyVehicle = new EmergencyVehicle();
        const authority = new Authority();
        authority.name = 'TestAuthority';

        emergencyVehicle.name = 'testName';
        emergencyVehicle.authority = authority;
        emergencyVehicle.state = EmergencyVehicleState.Active;
        emergencyVehicle.type = EmergencyVehicleType.Ambulance;

        const errors = await validate(emergencyVehicle);
        expect(errors.length).toEqual(0);
        done();
    });
});
