import { validate } from 'class-validator';
import { Authority } from '../../../src/api/models/Authority';

describe('AuthorityValidations', () => {
    test('Authority should always have a name', async done => {
        const authority = new Authority();
        const errorsOne = await validate(authority);
        authority.name = 'TestName';
        const errorsTwo = await validate(authority);
        expect(errorsOne.length).toBeGreaterThan(errorsTwo.length);
        done();
    });

    test('Authority validation should succeed with all required fields', async done => {
        const authority = new Authority();
        authority.name = 'test';
        const errors = await validate(authority);
        expect(errors.length).toEqual(0);
        done();
    });
});
