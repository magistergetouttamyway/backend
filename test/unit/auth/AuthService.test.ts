import { Request } from 'express';
import MockExpressRequest from 'mock-express-request';
import { User } from '../../../src/api/models/User';
import { AuthService } from '../../../src/auth/AuthService';
import { LogMock } from '../lib/LogMock';
import { RepositoryMock } from '../lib/RepositoryMock';
import { JsonWebTokenError } from 'jsonwebtoken';

describe('AuthService', () => {
    let authService: AuthService;
    let userRepository: RepositoryMock<User>;
    let log: LogMock;
    beforeEach(() => {
        log = new LogMock();
        userRepository = new RepositoryMock<User>();
        authService = new AuthService(log, userRepository as any);
    });

    describe('parseTokenFromRequest', () => {
        test('Should return the credentials of the basic authorization header', async () => {
            const user = new User();
            user.id = '1234';
            user.username = 'Bruce';
            user.password = '1234';
            user.email = 'bruce@wayne.com';

            const tokenResponse = await authService.generateJWTToken(user);
            const req: Request = new MockExpressRequest({
                headers: {
                    Authorization: `Basic ${tokenResponse.token}`,
                },
            });
            const tokenFromReq = authService.parseBasicAuthTokenFromRequest(req);
            const decoded = await authService.validateJWTToken(tokenFromReq);
            const credentials = decoded.user;

            expect(credentials.username).toBe('Bruce');
            expect(credentials.password).toBe('1234');
        });

        test('Should return undefined if there is no basic authorization header', () => {
            const req: Request = new MockExpressRequest({
                headers: {},
            });
            const token = authService.parseBasicAuthTokenFromRequest(req);
            expect(token).toBeUndefined();
            expect(log.infoMock).toBeCalledWith('No token provided by the client', []);
        });

        test('Should return undefined if there is a invalid basic authorization header', async () => {
            const req: Request = new MockExpressRequest({
                headers: {
                    Authorization: 'Basic 1234',
                },
            });
            const token = authService.parseBasicAuthTokenFromRequest(req);
            try {
                await authService.validateJWTToken(token);
            } catch (e) {
                expect(e).toBeInstanceOf(JsonWebTokenError);
            }

            expect(log.infoMock).toBeCalledWith('Invalid token provided by the client!', []);
        });
    });
});
