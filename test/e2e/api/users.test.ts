import * as nock from 'nock';
import request from 'supertest';
import { runSeed } from 'typeorm-seeding';

import { User } from '../../../src/api/models/User';
import { CreateBruce } from '../../../src/database/seeds/CreateBruce';
import { closeDatabase } from '../../utils/database';
import { BootstrapSettings } from '../utils/bootstrap';
import { prepareServer } from '../utils/server';
import { AuthService, TokenResponse } from '../../../src/auth/AuthService';
import { LogMock } from '../../unit/lib/LogMock';
import { RepositoryMock } from '../../unit/lib/RepositoryMock';

describe('/api/users', () => {
    let bruce: User;
    let settings: BootstrapSettings;
    let bruceTokenResponse: TokenResponse;
    let authService: AuthService;
    let userRepository: RepositoryMock<User>;
    let log: LogMock;

    // -------------------------------------------------------------------------
    // Setup up
    // -------------------------------------------------------------------------

    beforeAll(async done => {
        settings = await prepareServer({ migrate: true });
        log = new LogMock();
        userRepository = new RepositoryMock<User>();
        authService = new AuthService(log, userRepository as any);

        bruce = await runSeed<User>(CreateBruce);
        bruceTokenResponse = await authService.generateJWTToken(bruce);
        done();
    });

    // -------------------------------------------------------------------------
    // Tear down
    // -------------------------------------------------------------------------

    afterAll(async done => {
        nock.cleanAll();
        await closeDatabase(settings.connection);
        done();
    });

    // -------------------------------------------------------------------------
    // Test cases
    // -------------------------------------------------------------------------

    test('GET: / should return a list of users', async done => {
        const response = await request(settings.app)
            .get('/api/users')
            .set('Authorization', `Basic ${bruceTokenResponse.token}`)
            .expect('Content-Type', /json/)
            .expect(200);

        expect(response.body.length).toBe(1);
        done();
    });

    test('GET: /:id should return bruce', async done => {
        const response = await request(settings.app)
            .get(`/api/users/me`)
            .set('Authorization', `Basic ${bruceTokenResponse.token}`)
            .expect('Content-Type', /json/)
            .expect(200);

        expect(response.body.id).toBe(bruce.id);
        expect(response.body.username).toBe(bruce.username);
        expect(response.body.email).toBe(bruce.email);
        done();
    });
});
