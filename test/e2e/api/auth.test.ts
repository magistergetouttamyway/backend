import * as nock from 'nock';
import request from 'supertest';
import { runSeed } from 'typeorm-seeding';

import { User } from '../../../src/api/models/User';
import { CreateBruce } from '../../../src/database/seeds/CreateBruce';
import { closeDatabase } from '../../utils/database';
import { BootstrapSettings } from '../utils/bootstrap';
import { prepareServer } from '../utils/server';
import { AuthService } from '../../../src/auth/AuthService';
import { LogMock } from '../../unit/lib/LogMock';
import { RepositoryMock } from '../../unit/lib/RepositoryMock';

describe('/api/auth', () => {
    let bruce: User;
    let settings: BootstrapSettings;
    let authService: AuthService;
    let userRepository: RepositoryMock<User>;
    let log: LogMock;

    // -------------------------------------------------------------------------
    // Setup up
    // -------------------------------------------------------------------------

    beforeAll(async done => {
        settings = await prepareServer({ migrate: true });
        log = new LogMock();
        userRepository = new RepositoryMock<User>();
        authService = new AuthService(log, userRepository as any);

        bruce = await runSeed<User>(CreateBruce);
        done();
    });

    // -------------------------------------------------------------------------
    // Tear down
    // -------------------------------------------------------------------------

    afterAll(async done => {
        nock.cleanAll();
        await closeDatabase(settings.connection);
        done();
    });

    // -------------------------------------------------------------------------
    // Test cases
    // -------------------------------------------------------------------------

    test('Post: / user should login and token should be returned', async done => {
        const response = await request(settings.app)
            .post('/api/auth')
            .send({
                username: 'bruce',
                password: '1234',
            })
            .expect('Content-Type', /json/)
            .expect(200);
        const decodedResponse = await authService.validateJWTToken(response.body.token);
        expect(decodedResponse.user.id).toBe(bruce.id);
        done();
    });

    test('Post: /register new user should be created', async done => {
        const response = await request(settings.app)
            .post(`/api/auth/register`)
            .send({
                username: 'newUser',
                password: 'mypasswod',
                email: 'user@user.com',
                firstname: 'firstname',
                lastname: 'lastname',
            })
            .expect('Content-Type', /json/)
            .expect(200);

        expect(response.body.username).toBe('newuser'); // username should be lowered
        expect(response.body.email).toBe('user@user.com');
        expect(response.body).toHaveProperty('id');
        done();
    });
});
