import * as nock from 'nock';
import request from 'supertest';
import { closeDatabase } from '../../utils/database';
import { BootstrapSettings } from '../utils/bootstrap';
import { prepareServer } from '../utils/server';
import * as Turf from '@turf/turf';

describe('/api/map-utils', () => {
    let settings: BootstrapSettings;

    // -------------------------------------------------------------------------
    // Setup up
    // -------------------------------------------------------------------------

    beforeAll(async done => {
        settings = await prepareServer({ migrate: true });
        done();
    });

    // -------------------------------------------------------------------------
    // Tear down
    // -------------------------------------------------------------------------

    afterAll(async done => {
        nock.cleanAll();
        await closeDatabase(settings.connection);
        done();
    });

    // -------------------------------------------------------------------------
    // Test cases
    // -------------------------------------------------------------------------

    test('Post: /routes-intersect providing 2 intersecting routes a intersection point should be returned', async done => {
        const route1 = Turf.lineString([[20, 20], [40, 40]]);
        const route2 = Turf.lineString([[20, 40], [40, 20]]);
        const intersectionPoint = Turf.point([30, 30]);
        const response = await request(settings.app)
            .post('/api/map-utils/routes-intersect')
            .send({
                route1: route1,
                route2: route2,
            })
            .expect('Content-Type', /json/)
            .expect(200);
        expect(response.body.intersections).toHaveLength(1);
        expect(response.body.intersections[0]).toEqual(intersectionPoint.geometry);
        expect(response.body.overlappings).toHaveLength(0);
        done();
    });

    test('Post: /routes-intersect providing 2 non-intersecting routes no intersections and overlappings should be returned', async done => {
        const route1 = Turf.lineString([[20, 40], [40, 40]]);
        const route2 = Turf.lineString([[20, 20], [40, 20]]);
        const response = await request(settings.app)
            .post('/api/map-utils/routes-intersect')
            .send({
                route1: route1,
                route2: route2,
            })
            .expect('Content-Type', /json/)
            .expect(200);

        expect(response.body.intersections).toHaveLength(0);
        expect(response.body.overlappings).toHaveLength(0);
        done();
    });

    test('Post: /routes-intersect providing 2 overlapping routes no intersections and 1 overlapping should be returned', async done => {
        const route1 = Turf.lineString([[10, 40], [20, 20], [30, 20], [40, 40]]);
        const route2 = Turf.lineString([[10, 0], [20, 20], [30, 20], [40, 0]]);
        const response = await request(settings.app)
            .post('/api/map-utils/routes-intersect')
            .send({
                route1: route1,
                route2: route2,
            })
            .expect('Content-Type', /json/)
            .expect(200);

        expect(response.body.intersections).toHaveLength(0);
        expect(response.body.overlappings).toHaveLength(1);
        expect(response.body.overlappings[0].coordinates).toEqual([[20, 20], [30, 20]]);
        done();
    });

    test('Post: /routes-intersect providing overlapping and intersecting routes should contain 1 intersection and 1 overlapping', async done => {
        const route1 = Turf.lineString([[10, 40], [20, 20], [30, 20], [40, 40], [60, 0]]);
        const route2 = Turf.lineString([[10, 0], [20, 20], [30, 20], [40, 0], [60, 40]]);
        const response = await request(settings.app)
            .post('/api/map-utils/routes-intersect')
            .send({
                route1: route1,
                route2: route2,
            })
            .expect('Content-Type', /json/)
            .expect(200);

        expect(response.body.intersections).toHaveLength(1);
        expect(response.body.intersections[0].coordinates).toEqual([50, 20]);
        expect(response.body.overlappings).toHaveLength(1);
        expect(response.body.overlappings[0].coordinates).toEqual([[20, 20], [30, 20]]);
        done();
    });
});
