import { Column, Entity, JoinColumn, OneToOne } from 'typeorm';
import { User } from './User';
import { IsNotEmpty } from 'class-validator';
import { Location } from './Location';
import { BaseEntity } from './abstract/BaseEntity';

@Entity({ name: 'user_client' })
export class Client extends BaseEntity {
    @OneToOne(type => User, user => user.client)
    public user: User;

    @IsNotEmpty()
    @Column({ name: 'first_name' })
    public firstName: string;

    @IsNotEmpty()
    @Column({ name: 'last_name' })
    public lastName: string;

    @OneToOne(type => Location, { cascade: true })
    @JoinColumn()
    public location: Location;
}
