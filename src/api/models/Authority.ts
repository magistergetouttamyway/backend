import { Column, Entity, OneToMany, OneToOne } from 'typeorm';
import { User } from './User';
import { IsNotEmpty } from 'class-validator';
import { EmergencyVehicle } from './EmergencyVehicle';
import { BaseEntity } from './abstract/BaseEntity';

@Entity({ name: 'user_authority' })
export class Authority extends BaseEntity {
    @OneToOne(type => User, user => user.authority)
    public user: User;

    @IsNotEmpty()
    @Column({ name: 'name' })
    public name: string;

    @OneToMany(type => EmergencyVehicle, emergencyVehicle => emergencyVehicle.authority)
    public emergencyVehicles: EmergencyVehicle[];
}
