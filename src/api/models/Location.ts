import { Column, Entity, Index } from 'typeorm';
import { LineString, Point } from 'geojson';
import { BaseEntity } from './abstract/BaseEntity';

@Entity()
export class Location extends BaseEntity {
    @Column('geometry', { spatialFeatureType: 'Point', nullable: true })
    @Index({ spatial: true })
    public currentLocation: Point;

    @Column('geometry', { spatialFeatureType: 'LineString', nullable: true })
    @Index({ spatial: true })
    public route: LineString;

    public toString(): string {
        return `${this.currentLocation} ${this.route}`;
    }
}
