import * as bcrypt from 'bcrypt';
import { Exclude } from 'class-transformer';
import { IsNotEmpty } from 'class-validator';
import { BeforeInsert, Column, Entity, JoinColumn, OneToOne } from 'typeorm';
import { BaseEntity } from './abstract/BaseEntity';
import { Client } from './Client';
import { Authority } from './Authority';

@Entity()
export class User extends BaseEntity {
    public static hashPassword(password: string): Promise<string> {
        return new Promise((resolve, reject) => {
            bcrypt.hash(password, 10, (err, hash) => {
                if (err) {
                    return reject(err);
                }
                resolve(hash);
            });
        });
    }

    public static comparePassword(user: User, password: string): Promise<boolean> {
        return new Promise((resolve, reject) => {
            bcrypt.compare(password, user.password, (err, res) => {
                resolve(res === true);
            });
        });
    }

    @IsNotEmpty()
    @Column()
    public email: string;

    @IsNotEmpty()
    @Column({ select: false })
    @Exclude()
    public password: string;

    @IsNotEmpty()
    @Column()
    public username: string;

    @Column({ name: 'is_active', default: false })
    public isActive: boolean;

    @Column({ name: 'socket_id', default: undefined, nullable: true })
    public socketId: string;

    @OneToOne(type => Client, client => client.user, { nullable: true, cascade: true })
    @JoinColumn()
    public client: Client;

    @OneToOne(type => Authority, authority => authority.user, { nullable: true, cascade: true })
    @JoinColumn()
    public authority: Authority;

    public toString(): string {
        return `${this.id} (${this.email})`;
    }

    @BeforeInsert()
    public async hashPassword(): Promise<void> {
        this.password = await User.hashPassword(this.password);
    }

    public isAuthority(): boolean {
        return this.authority !== null;
    }

    public isClient(): boolean {
        return this.client !== null;
    }
}
