import { IsNotEmpty } from 'class-validator';
import { Column, Entity, JoinColumn, ManyToOne, OneToOne } from 'typeorm';
import { Authority } from './Authority';
import { Location } from './Location';
import { BaseEntity } from './abstract/BaseEntity';

export enum EmergencyVehicleState {
    Active,
    Inactive,
}

export enum EmergencyVehicleType {
    Ambulance = 'Ambulance',
    FireTruck = 'FireTruck',
    Police = 'Police',
}

@Entity()
export class EmergencyVehicle extends BaseEntity {
    @IsNotEmpty()
    @Column({ name: 'name' })
    public name: string;

    @IsNotEmpty()
    @Column({ name: 'type' })
    public type: EmergencyVehicleType;

    @IsNotEmpty()
    @ManyToOne(type => Authority, authority => authority.emergencyVehicles)
    public authority: Authority;

    @IsNotEmpty()
    @Column({ name: 'state', default: EmergencyVehicleState.Inactive })
    public state: EmergencyVehicleState;

    @OneToOne(type => Location, { cascade: true })
    @JoinColumn()
    public location: Location;

    public toString(): string {
        return `${this.name} ${this.type} (${this.location})`;
    }
}
