import { PrimaryColumn } from 'typeorm';

export abstract class BaseEntity {
    @PrimaryColumn('uuid')
    public id: string;
}
