import { ConnectedSocket, MessageBody, OnConnect, OnDisconnect, OnMessage, SocketController } from 'socket-controllers';
import { Logger, LoggerInterface } from '../../decorators/Logger';
import { EmergencyVehicleService } from '../services/EmergencyVehicleService';
import { Location } from '../models/Location';
import { LineString, Point } from 'geojson';
import { EmergencyVehicle, EmergencyVehicleState } from '../models/EmergencyVehicle';
import { BroadcastingService } from '../services/BroadcastingService';
import { ISocket } from '../socketMiddlewares/SocketAuthMiddleware';
import { UserService } from '../services/UserService';
import { User } from '../models/User';

interface IEmergencyVehicleLocationUpdateMessage {
    currentLocation: Point;
    emergencyVehicleId: string;
    route: LineString;
}

interface IEmergencyStateUpdateMessage {
    emergencyVehicleId: string;
    state: EmergencyVehicleState;
}

@SocketController()
export class EmergencyVehicleController {
    constructor(
        private emergencyVehicleService: EmergencyVehicleService,
        private broadcastingService: BroadcastingService,
        private userService: UserService,
        @Logger(__filename) private log: LoggerInterface
    ) {}

    @OnConnect()
    public connection(@ConnectedSocket() socket: any): void {
        this.log.info('Client connected!');
    }

    @OnDisconnect()
    public disconnect(@ConnectedSocket() socket: any): void {
        this.log.info('Client disconnected!');
    }

    @OnMessage('emergency-vehicle-update-location')
    public async updateEmergencyVehicleLocation(
        @ConnectedSocket() socket: ISocket,
        @MessageBody() message: IEmergencyVehicleLocationUpdateMessage
    ): Promise<void> {
        this.log.info('Received Emergency Vehicle Location Message:');

        const user = await this.userService.findOne(socket.decodedToken.user.id);

        if (!user.isAuthority()) {
            return undefined;
        }

        if (
            message.currentLocation !== undefined &&
            message.emergencyVehicleId !== undefined &&
            message.route !== undefined
        ) {
            const newLocation = new Location();
            newLocation.currentLocation = message.currentLocation;
            newLocation.route = message.route;

            const emergencyVehicle = await this.emergencyVehicleService.findOne(message.emergencyVehicleId);

            if (!this.userIsAllowedToModifiyEmergencyVehicleData(user, emergencyVehicle)) {
                return undefined;
            }

            const updatedEmergencyVehicle = await this.emergencyVehicleService.updateLocation(
                emergencyVehicle,
                newLocation
            );

            if (updatedEmergencyVehicle.state === EmergencyVehicleState.Active) {
                return this.broadcastingService.broadcastEmergencyVehicleLocation(updatedEmergencyVehicle, socket);
            }
        }
        return undefined;
    }

    @OnMessage('emergency-vehicle-state')
    public async updateEmergencyVehicleState(
        @ConnectedSocket() socket: ISocket,
        @MessageBody() message: IEmergencyStateUpdateMessage
    ): Promise<EmergencyVehicle> {
        const user = await this.userService.findOne(socket.decodedToken.user.id);

        if (!user.isAuthority()) {
            return undefined;
        }

        if (message.emergencyVehicleId !== undefined && message.state !== undefined) {
            const emergencyVehicle = await this.emergencyVehicleService.findOne(message.emergencyVehicleId);
            if (!this.userIsAllowedToModifiyEmergencyVehicleData(user, emergencyVehicle)) {
                return undefined;
            }
            if (Number(message.state) === EmergencyVehicleState.Active) {
                await this.broadcastingService.broadcastEmergencyVehicleLocation(emergencyVehicle, socket);
            } else {
                this.broadcastingService.stopBroadcasting(emergencyVehicle, socket);
            }
            return this.emergencyVehicleService.updateState(emergencyVehicle, message.state);
        }
        return undefined;
    }

    private userIsAllowedToModifiyEmergencyVehicleData(user: User, emergencyVehicle: EmergencyVehicle): boolean {
        return user.authority.id === emergencyVehicle.authority.id;
    }
}
