import { ConnectedSocket, MessageBody, OnDisconnect, OnMessage, SocketController } from 'socket-controllers';
import { LineString, Point } from 'geojson';
import { ClientService } from '../services/ClientService';
import { Logger, LoggerInterface } from '../../decorators/Logger';
import { ISocket } from '../socketMiddlewares/SocketAuthMiddleware';
import { Location } from '../models/Location';
import { Client } from '../models/Client';
import { UserService } from '../services/UserService';
import { User } from '../models/User';

interface IClientLocationMessage {
    currentLocation?: Point;
    route?: LineString;
}

interface IClientLeaveRoomMessage {
    roomId: string;
}

@SocketController()
export class ClientController {
    constructor(
        private userService: UserService,
        private clientService: ClientService,
        @Logger(__filename) private log: LoggerInterface
    ) {}

    @OnDisconnect()
    public async disconnect(@ConnectedSocket() socket: ISocket): Promise<User> {
        this.log.info('Client disconnected!');
        const user = await this.userService.findOne(socket.decodedToken.user.id);
        return this.userService.updateSocketInformation(user, null, false);
    }

    @OnMessage('client-location-update')
    public async updateClientLocation(
        @ConnectedSocket() socket: ISocket,
        @MessageBody() message: IClientLocationMessage
    ): Promise<Client> {
        this.log.info('Received Client Location Update Message');
        const user = await this.userService.findOne(socket.decodedToken.user.id);

        if (!user.isClient()) {
            return undefined;
        }

        // Has location
        const client = await this.clientService.findOne(user.client.id);

        if (message.currentLocation !== undefined) {
            const newLocation = new Location();
            newLocation.currentLocation = message.currentLocation;
            return this.clientService.updateLocation(client, newLocation);
        }
        return undefined;
    }

    @OnMessage('client-leave-room')
    public async leaveRoom(
        @ConnectedSocket() socket: ISocket,
        @MessageBody() message: IClientLeaveRoomMessage
    ): Promise<void> {
        this.log.info('Received Client leave room event');
        const user = await this.userService.findOne(socket.decodedToken.user.id);

        if (!user.isClient()) {
            return;
        }

        if (message && message.roomId !== undefined) {
            socket.leave(message.roomId);
        }

        return;
    }
}
