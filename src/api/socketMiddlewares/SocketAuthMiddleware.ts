import { Middleware, MiddlewareInterface } from 'socket-controllers';
import * as socketioJwt from 'socketio-jwt';
import { Packet, Socket } from 'socket.io';
import { Logger, LoggerInterface } from '../../decorators/Logger';
import { UserService } from '../services/UserService';
import { User } from '../models/User';
import { env } from '../../env';

export interface ISocket extends Socket {
    decodedToken: Token;
}

interface Token {
    user: User;
}

@Middleware()
export class SocketAuthMiddleWare implements MiddlewareInterface {
    private authenticatedListenerHasBeenAdded = false;

    constructor(@Logger(__filename) private log: LoggerInterface, private userService: UserService) {}

    public use(socket: ISocket, next: (err?: any) => any): void {
        socketioJwt.authorize({
            secret: env.auth.jwtTokenSecret,
            timeout: 15000, // 15 seconds to send the authentication message
            decodedPropertyName: 'decodedToken',
            callback: true, // Disconnect when token is invalid
        })(socket);

        socket.use((packet: Packet, socketNext) => {
            if (!this.authTokenExists(socket)) {
                this.allowAuthenticationPacket(socket, packet, socketNext);
                return;
            }
            socketNext();
        });

        if (!this.authenticatedListenerHasBeenAdded) {
            this.authenticatedListenerHasBeenAdded = true;
            socket.server.on('authenticated', async (s: ISocket) => {
                this.log.info('Client authenticated, id: ' + s.decodedToken.user.id);
                const user = await this.userService.findOne(s.decodedToken.user.id);
                if (user !== undefined) {
                    this.userService.updateSocketInformation(user, s.id, true);
                } else {
                    s.decodedToken = undefined;
                }
            });
        }

        next();
    }

    private allowAuthenticationPacket(socket: Socket, packet: Packet, next: (error?: Error) => void): void {
        const packetName = packet[0];

        if (packetName === 'authenticate') {
            next();
            return;
        }
        const error = new Error('Unauthorized Access');

        socket.emit('unauthorized', error, () => {
            socket.disconnect(true);
        });
        next(error);
    }

    private authTokenExists(socket: ISocket): boolean {
        return socket.decodedToken !== undefined;
    }
}
