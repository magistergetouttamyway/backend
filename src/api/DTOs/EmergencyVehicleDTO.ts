import { Point } from 'geojson';
import { LocationDTO } from './LocationDTO';
import { EmergencyVehicle } from '../models/EmergencyVehicle';

export class EmergencyVehicleDTO {
    public id: string;
    public name: string;
    public type: string;
    public location: LocationDTO;
    public intersections: Point[];

    constructor(emergencyVehicle: EmergencyVehicle) {
        this.id = emergencyVehicle.id;
        this.name = emergencyVehicle.name;
        this.type = emergencyVehicle.type;
        this.location = new LocationDTO(emergencyVehicle.location);
        this.intersections = [];
    }
}
