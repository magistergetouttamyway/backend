import { LineString, Point } from 'geojson';

export class IntersectionsDTO {
    public intersections: Point[];
    public overlappings: LineString[];

    constructor() {
        this.intersections = [];
        this.overlappings = [];
    }

    public setIntersections(intersections: Point[]): void {
        this.intersections = intersections;
    }

    public setOverlappings(overlappings: LineString[]): void {
        this.overlappings = overlappings;
    }
}
