import { LineString, Point } from 'geojson';
import { Location } from '../models/Location';

export class LocationDTO {
    public id: string;
    public currentLocation: Point;
    public route: LineString;

    constructor(location: Location) {
        this.id = location.id;
        this.currentLocation = location.currentLocation;
        this.route = location.route;
    }
}
