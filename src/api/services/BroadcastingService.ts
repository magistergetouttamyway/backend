import { Service } from 'typedi';
import { Logger, LoggerInterface } from '../../decorators/Logger';
import { EmergencyVehicle } from '../models/EmergencyVehicle';
import { ISocket } from '../socketMiddlewares/SocketAuthMiddleware';
import { EmergencyVehicleDTO } from '../DTOs/EmergencyVehicleDTO';
import { ClientService } from './ClientService';

@Service()
export class BroadcastingService {
    constructor(@Logger(__filename) private log: LoggerInterface, private clientService: ClientService) {}

    public async broadcastEmergencyVehicleLocation(emergencyVehicle: EmergencyVehicle, socket: ISocket): Promise<void> {
        this.log.info('Broadcasting, vehicle id: ' + emergencyVehicle.id);

        // 1. Find nearby users & add them to room
        const nearbyClients = await this.clientService.findActiveAppUsersNearEmergencyVehicle(emergencyVehicle, 4000);
        const sockets = socket.server.sockets.connected;
        for (const client of nearbyClients) {
            const userSocket = sockets[client.user.socketId];

            if (userSocket !== undefined) {
                userSocket.join(emergencyVehicle.id);
            }
        }

        // 2. Broadcast location
        const dto = new EmergencyVehicleDTO(emergencyVehicle);
        socket.broadcast.to(emergencyVehicle.id).emit('emergency-vehicle-nearby', dto);
    }

    public stopBroadcasting(emergencyVehicle: EmergencyVehicle, socket: ISocket): void {
        this.log.info('Stopped Location broadcasting, vehicle id: ' + emergencyVehicle.id);

        // 1. Broadcast alarmed vehicle deactivated state.
        const dto = new EmergencyVehicleDTO(emergencyVehicle);
        socket.broadcast.emit('emergency-vehicle-deactivated', dto);

        // 2. Clear the room.
        socket.server.in(emergencyVehicle.id).clients((error, socketIds) => {
            if (error) {
                throw error;
            }
            const clientSockets = socket.server.sockets.connected;
            socketIds.forEach(
                socketId => clientSockets[socketId] && clientSockets[socketId].leave(emergencyVehicle.id)
            );
        });
    }
}
