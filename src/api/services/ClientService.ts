import { Service } from 'typedi';
import { OrmRepository } from 'typeorm-typedi-extensions';
import uuid from 'uuid';

import { EventDispatcher, EventDispatcherInterface } from '../../decorators/EventDispatcher';
import { Logger, LoggerInterface } from '../../decorators/Logger';
import { ClientRepository } from '../repositories/ClientRepository';
import { events } from '../subscribers/events';
import { Location } from '../models/Location';
import { LocationService } from './LocationService';
import { getManager } from 'typeorm';
import { EmergencyVehicle } from '../models/EmergencyVehicle';
import { Client } from '../models/Client';

@Service()
export class ClientService {
    constructor(
        private locationService: LocationService,
        @OrmRepository() private clientRepository: ClientRepository,
        @EventDispatcher() private eventDispatcher: EventDispatcherInterface,
        @Logger(__filename) private log: LoggerInterface
    ) {}

    public find(): Promise<Client[]> {
        this.log.info('Find all clients');
        return this.clientRepository.find({ relations: ['location'] });
    }

    public findOne(id: string): Promise<Client | undefined> {
        this.log.info('Find one client');
        return this.clientRepository.findOne({ id }, { relations: ['location'] });
    }

    public findActiveAppUsersNearEmergencyVehicle(
        emergencyVehicle: EmergencyVehicle,
        radiusInMeters: number
    ): Promise<Client[]> {
        this.log.info('Find active clients');
        const manager = getManager();
        if (
            emergencyVehicle &&
            emergencyVehicle.location &&
            emergencyVehicle.location.currentLocation &&
            emergencyVehicle.location.currentLocation.coordinates &&
            emergencyVehicle.location.currentLocation.coordinates[0] !== undefined &&
            emergencyVehicle.location.currentLocation.coordinates[1] !== undefined
        ) {
            const nearbyClients = manager
                .createQueryBuilder()
                .select('client')
                .from(Client, 'client')
                .leftJoinAndSelect('client.location', 'cl')
                .leftJoinAndSelect('client.user', 'cu')
                .where(
                    'ST_DWithin(cl."currentLocation"::geography, ST_MakePoint(:latitude, :longitude)::geography, :radius)',
                    {
                        latitude: emergencyVehicle.location.currentLocation.coordinates[0],
                        longitude: emergencyVehicle.location.currentLocation.coordinates[1],
                        radius: radiusInMeters,
                    }
                )
                .andWhere('cu.isActive = true');
            return nearbyClients.getMany();
        }
        return new Promise<Client[]>(resolve => resolve([]));
    }

    public async create(client: Client): Promise<Client> {
        this.log.info('Create a new user => ', client.toString());
        client.id = uuid.v1();
        const newClient = await this.clientRepository.save(client);
        this.eventDispatcher.dispatch(events.user.created, newClient);
        return newClient;
    }

    public update(id: string, client: Client): Promise<Client> {
        this.log.info('Update a user');
        client.id = id;
        return this.clientRepository.save(client);
    }

    public async updateLocation(client: Client, location: Location): Promise<Client> {
        if (client.location === undefined || client.location === null) {
            client.location = await this.locationService.create(location);
        } else {
            client.location = await this.locationService.update(client.location.id, location);
        }
        return this.clientRepository.save(client);
    }

    public async delete(id: string): Promise<void> {
        this.log.info('Delete a client');
        await this.clientRepository.delete(id);
        return;
    }
}
