import { Service } from 'typedi';
import uuid from 'uuid';
import { Logger, LoggerInterface } from '../../decorators/Logger';
import { OrmRepository } from 'typeorm-typedi-extensions';
import { LocationRepository } from '../repositories/LocationRepository';
import { Location } from '../models/Location';

@Service()
export class LocationService {
    constructor(
        @OrmRepository() private locationRepository: LocationRepository,
        @Logger(__filename) private log: LoggerInterface
    ) {}

    public async update(id: string, location: Location): Promise<Location> {
        this.log.info('Update location');
        location.id = id;
        return this.locationRepository.save(location);
    }

    public async create(location: Location): Promise<Location> {
        this.log.info('Create new location');
        location.id = uuid.v1();
        return this.locationRepository.save(location);
    }
}
