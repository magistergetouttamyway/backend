import { Logger, LoggerInterface } from '../../decorators/Logger';
import { Service } from 'typedi';
import { LineString } from 'geojson';
import * as Turf from '@turf/turf';
import { IntersectionsDTO } from '../DTOs/IntersectionsDTO';

// const overlappingTolerance = 1 / 100; // 1km / 100 => 10m

@Service()
export class MapUtilsService {
    constructor(@Logger(__filename) private log: LoggerInterface) {}

    public findRoutesIntersectionsAndOverlappings(route1: LineString, route2: LineString): IntersectionsDTO {
        this.log.info('Route intersection called');

        const intersectionDTO = new IntersectionsDTO();

        if (route1 === undefined || route2 === undefined) {
            return intersectionDTO;
        }

        const intersections = Turf.lineIntersect(route1, route2).features.map(feature => feature.geometry);
        const overlappings = Turf.lineOverlap(route1, route2, { tolerance: 0.1 }).features.map(
            feature => feature.geometry
        );

        const newIntersections = [];

        // IF point exists on overlapping remove it.
        for (const intersection of intersections) {
            let pointOnLine = false;
            for (const overlapping of overlappings) {
                if (Turf.booleanPointOnLine(intersection, overlapping, { ignoreEndVertices: false })) {
                    pointOnLine = true;
                    break;
                }
            }

            if (!pointOnLine) {
                newIntersections.push(intersection);
            }
        }

        intersectionDTO.setIntersections(newIntersections);
        intersectionDTO.setOverlappings(overlappings);

        return intersectionDTO;
    }
}
