import { Service } from 'typedi';
import { Logger, LoggerInterface } from '../../decorators/Logger';
import { OrmRepository } from 'typeorm-typedi-extensions';
import { EmergencyVehicleRepository } from '../repositories/EmergencyVehicleRepository';
import { EmergencyVehicle, EmergencyVehicleState } from '../models/EmergencyVehicle';
import { LocationService } from './LocationService';
import { Location } from '../models/Location';

@Service()
export class EmergencyVehicleService {
    constructor(
        private locationService: LocationService,
        @OrmRepository() private emergencyVehicleRepository: EmergencyVehicleRepository,
        @Logger(__filename) private log: LoggerInterface
    ) {}

    public async findOne(id: string): Promise<EmergencyVehicle> {
        this.log.info('Find single emergency vehicle');
        return this.emergencyVehicleRepository.findOne({ id }, { relations: ['location', 'authority'] });
    }

    public async updateLocation(emergencyVehicle: EmergencyVehicle, location: Location): Promise<EmergencyVehicle> {
        if (emergencyVehicle.location === undefined || emergencyVehicle.location === null) {
            emergencyVehicle.location = await this.locationService.create(location);
        } else {
            emergencyVehicle.location = await this.locationService.update(emergencyVehicle.location.id, location);
        }
        return this.emergencyVehicleRepository.save(emergencyVehicle);
    }

    public async updateState(
        emergencyVehicle: EmergencyVehicle,
        state: EmergencyVehicleState
    ): Promise<EmergencyVehicle> {
        this.log.info('Update emergency vehicle state');
        emergencyVehicle.state = state;
        return this.emergencyVehicleRepository.save(emergencyVehicle);
    }
}
