import { Service } from 'typedi';
import { OrmRepository } from 'typeorm-typedi-extensions';
import uuid from 'uuid';

import { EventDispatcher, EventDispatcherInterface } from '../../decorators/EventDispatcher';
import { Logger, LoggerInterface } from '../../decorators/Logger';
import { events } from '../subscribers/events';
import { UserRepository } from '../repositories/UserRepository';
import { User } from '../models/User';
import { Client } from '../models/Client';
import { ClientRepository } from '../repositories/ClientRepository';

@Service()
export class UserService {
    constructor(
        @OrmRepository() private userRepository: UserRepository,
        @OrmRepository() private clientRepository: ClientRepository,
        @EventDispatcher() private eventDispatcher: EventDispatcherInterface,
        @Logger(__filename) private log: LoggerInterface
    ) {}

    public find(): Promise<User[]> {
        this.log.info('Find all users');
        return this.userRepository.find({ relations: ['client', 'authority'] });
    }

    public findOne(id: string): Promise<User | undefined> {
        this.log.info('Find one user');
        return this.userRepository.findOne({ id }, { relations: ['client', 'authority'] });
    }

    public async create(user: User): Promise<User> {
        this.log.info('Create a new user => ', user.toString());
        user.id = uuid.v1();
        user.username = user.username.toLowerCase();
        const newUser = await this.userRepository.save(user);
        this.eventDispatcher.dispatch(events.user.created, newUser);
        return newUser;
    }

    public async createClient(user: User, client: Client): Promise<User> {
        this.log.info('Create a new client => ', user.toString());

        await this.validateClient(user, client);
        user.id = uuid.v1();
        user.username = user.username.toLowerCase();
        const newUser = await this.userRepository.save(user);

        client.id = uuid.v1();
        client.user = newUser;
        await this.clientRepository.save(client);

        this.eventDispatcher.dispatch(events.user.created, newUser);
        return newUser;
    }

    public update(id: string, user: User): Promise<User> {
        this.log.info('Update a user');
        user.id = id;
        return this.userRepository.save(user);
    }

    public updateSocketInformation(user: User, socketId: string, isActive: boolean): Promise<User> {
        user.socketId = socketId;
        user.isActive = isActive;
        return this.userRepository.save(user);
    }

    public async delete(id: string): Promise<void> {
        this.log.info('Delete a user');
        await this.userRepository.delete(id);
        return;
    }

    private async validateClient(user: User, client: Client): Promise<void> {
        if (user.username === undefined) {
            throw new Error('Username missing');
        }
        const existingUsername = await this.userRepository.findOne({ username: user.username });

        if (existingUsername !== undefined) {
            throw new Error('Username already exists');
        }

        const existingEmail = await this.userRepository.findOne({ email: user.email });

        if (user.email === undefined || existingEmail !== undefined) {
            throw new Error('Email already exists or is missing');
        }

        if (user.password === undefined) {
            throw new Error('Password missing');
        }

        if (client.firstName === undefined) {
            throw new Error('Firstname missing');
        }

        if (client.lastName === undefined) {
            throw new Error('Lastname missing');
        }
    }
}
