import { EntityRepository, Repository } from 'typeorm';
import { EmergencyVehicle } from '../models/EmergencyVehicle';

@EntityRepository(EmergencyVehicle)
export class EmergencyVehicleRepository extends Repository<EmergencyVehicle> {}
