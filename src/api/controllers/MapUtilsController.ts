import { Body, JsonController, Post } from 'routing-controllers';
import { LineString } from 'geojson';
import { MapUtilsService } from '../services/MapUtilsService';
import { IntersectionsDTO } from '../DTOs/IntersectionsDTO';

interface IRoutesIntersections {
    route1: LineString;
    route2: LineString;
}

// @Authorized() Comment in when ready
@JsonController('/map-utils')
export class MapUtilsController {
    constructor(private mapUtilsService: MapUtilsService) {}

    @Post('/routes-intersect')
    public findRoutesIntersections(@Body() body: IRoutesIntersections): IntersectionsDTO {
        return this.mapUtilsService.findRoutesIntersectionsAndOverlappings(body.route1, body.route2);
    }
}
