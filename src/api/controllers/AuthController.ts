import { Body, JsonController, Post, Res } from 'routing-controllers';
import { AuthService } from '../../auth/AuthService';
import { User } from '../models/User';
import { UserService } from '../services/UserService';
import { Client } from '../models/Client';

interface ICredentials {
    username: string;
    password: string;
}

interface IClientData {
    email: string;
    username: string;
    password: string;
    firstname: string;
    lastname: string;
}

@JsonController('/auth')
export class UserController {
    constructor(private authService: AuthService, private userService: UserService) {}

    @Post()
    public async logIn(@Body() credentials: ICredentials, @Res() response: any): Promise<any> {
        const user = await this.authService.validateUser(credentials.username, credentials.password);

        if (user !== undefined) {
            return this.authService.generateJWTToken(user);
        }

        return undefined;
    }

    @Post('/register')
    public create(@Body() clientData: IClientData): Promise<User> {
        const user = new User();
        user.email = clientData.email;
        user.username = clientData.username;
        user.password = clientData.password;

        const client = new Client();
        client.firstName = clientData.firstname;
        client.lastName = clientData.lastname;

        return this.userService.createClient(user, client);
    }
}
