import * as express from 'express';
import { Service } from 'typedi';
import { OrmRepository } from 'typeorm-typedi-extensions';
import jwt from 'jsonwebtoken';
import { User } from '../api/models/User';
import { UserRepository } from '../api/repositories/UserRepository';
import { Logger, LoggerInterface } from '../decorators/Logger';
import { env } from '../env';
import { NotFoundError } from 'routing-controllers';
export interface TokenResponse {
    token: string;
}

@Service()
export class AuthService {
    constructor(
        @Logger(__filename) private log: LoggerInterface,
        @OrmRepository() private userRepository: UserRepository
    ) {}

    public parseBasicAuthTokenFromRequest(req: express.Request): string {
        const authorization = req.header('authorization');

        if (authorization && authorization.split(' ')[0] === 'Basic') {
            this.log.info('Token provided by the client');
            return authorization.split(' ')[1];
        }

        this.log.info('No token provided by the client');
        return undefined;
    }

    public async validateUser(username: string, password: string): Promise<User> {
        const usernameLower = username.toLowerCase();
        const user = await this.userRepository.findOne({
            select: ['id', 'username', 'password', 'email'],
            where: {
                username: usernameLower,
            },
        });

        if (user === undefined) {
            throw new NotFoundError('Invalid Credentials');
        }

        if (await User.comparePassword(user, password)) {
            return user;
        }

        throw new NotFoundError('Invalid Credentials');
    }

    public generateJWTToken(user: User): Promise<TokenResponse> {
        return new Promise<TokenResponse>((resolve, reject) => {
            jwt.sign({ user }, env.auth.jwtTokenSecret, {}, (err, token) => {
                if (err) {
                    reject(err);
                } else {
                    resolve({ token });
                }
            });
        });
    }

    public validateJWTToken(token: string): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            jwt.verify(token, env.auth.jwtTokenSecret, (err, authorizedData) => {
                if (err) {
                    this.log.info('Invalid token provided by the client!');
                    reject(err);
                } else {
                    resolve(authorizedData);
                }
            });
        });
    }
}
