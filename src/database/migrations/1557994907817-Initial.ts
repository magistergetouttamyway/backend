import { MigrationInterface, QueryRunner } from 'typeorm';

export class Initial1557994907817 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('CREATE EXTENSION IF NOT EXISTS postgis');
        await queryRunner.query(`CREATE TABLE "location" ("id" uuid NOT NULL, "currentLocation" geometry(Point),
         "route" geometry(LineString), CONSTRAINT "PK_876d7bdba03c72251ec4c2dc827" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE INDEX "IDX_67aa7f9065aaa7541cf47002a8" ON "location" USING GiST
          ("currentLocation") `);
        await queryRunner.query(`CREATE INDEX "IDX_0972a5f1cfc41e25b4db7b0066" ON "location" USING GiST
          ("route") `);
        await queryRunner.query(`CREATE TABLE "user_client" ("id" uuid NOT NULL, "first_name" character
         varying NOT NULL, "last_name" character varying NOT NULL, "locationId" uuid, CONSTRAINT
          "REL_e09cc49bd402432a31d40e02ad" UNIQUE ("locationId"), CONSTRAINT "PK_fed09e6f9c08a5031ba1e3e9701"
           PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "user" ("id" uuid NOT NULL, "email" character varying
         NOT NULL, "password" character varying NOT NULL, "username" character varying NOT NULL, "is_active"
          boolean NOT NULL DEFAULT false, "socket_id" character varying, "clientId" uuid, "authorityId" uuid,
           CONSTRAINT "REL_56f28841fe433cf13f8685f9bc" UNIQUE ("clientId"), CONSTRAINT "REL_6c19d5938f86c648f46a4a2824"
            UNIQUE ("authorityId"), CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "emergency_vehicle" ("id" uuid NOT NULL, "name" character
         varying NOT NULL, "type" character varying NOT NULL, "state" integer NOT NULL DEFAULT 1, "authorityId"
          uuid, "locationId" uuid, CONSTRAINT "REL_387edda58e35e92ff57b1343fa" UNIQUE ("locationId"), CONSTRAINT
           "PK_98e98880dc0b4fe9f3c6b19054f" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "user_authority" ("id" uuid NOT NULL, "name" character
         varying NOT NULL, CONSTRAINT "PK_ffbf7cb34de4580893568f58e1f" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "user_client" ADD CONSTRAINT "FK_e09cc49bd402432a31d40e02ad7"
         FOREIGN KEY ("locationId") REFERENCES "location"("id")`);
        await queryRunner.query(`ALTER TABLE "user" ADD CONSTRAINT "FK_56f28841fe433cf13f8685f9bc1"
         FOREIGN KEY ("clientId") REFERENCES "user_client"("id")`);
        await queryRunner.query(`ALTER TABLE "user" ADD CONSTRAINT "FK_6c19d5938f86c648f46a4a28247"
         FOREIGN KEY ("authorityId") REFERENCES "user_authority"("id")`);
        await queryRunner.query(`ALTER TABLE "emergency_vehicle" ADD CONSTRAINT "FK_e91bbfd3cea9e02c3d5bd231a9f"
         FOREIGN KEY ("authorityId") REFERENCES "user_authority"("id")`);
        await queryRunner.query(`ALTER TABLE "emergency_vehicle" ADD CONSTRAINT "FK_387edda58e35e92ff57b1343fa5"
         FOREIGN KEY ("locationId") REFERENCES "location"("id")`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "emergency_vehicle" DROP CONSTRAINT "FK_387edda58e35e92ff57b1343fa5"`);
        await queryRunner.query(`ALTER TABLE "emergency_vehicle" DROP CONSTRAINT "FK_e91bbfd3cea9e02c3d5bd231a9f"`);
        await queryRunner.query(`ALTER TABLE "user" DROP CONSTRAINT "FK_6c19d5938f86c648f46a4a28247"`);
        await queryRunner.query(`ALTER TABLE "user" DROP CONSTRAINT "FK_56f28841fe433cf13f8685f9bc1"`);
        await queryRunner.query(`ALTER TABLE "user_client" DROP CONSTRAINT "FK_e09cc49bd402432a31d40e02ad7"`);
        await queryRunner.query(`DROP TABLE "user_authority"`);
        await queryRunner.query(`DROP TABLE "emergency_vehicle"`);
        await queryRunner.query(`DROP TABLE "user"`);
        await queryRunner.query(`DROP TABLE "user_client"`);
        await queryRunner.query(`DROP INDEX "IDX_0972a5f1cfc41e25b4db7b0066"`);
        await queryRunner.query(`DROP INDEX "IDX_67aa7f9065aaa7541cf47002a8"`);
        await queryRunner.query(`DROP TABLE "location"`);
    }
}
