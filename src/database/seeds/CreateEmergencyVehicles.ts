import { Connection } from 'typeorm';
import { Factory, Seed } from 'typeorm-seeding';
import * as uuid from 'uuid';

import { EmergencyVehicle, EmergencyVehicleType } from '../../api/models/EmergencyVehicle';
import { Authority } from '../../api/models/Authority';
import { User } from '../../api/models/User';

export class CreateEmergencyVehicles implements Seed {
    public async seed(factory: Factory, connection: Connection): Promise<Authority> {
        const em = connection.createEntityManager();

        const user = new User();
        user.id = uuid.v1();
        user.email = 'government@tartu.ee';
        user.username = 'government';
        user.password = '1234';

        const authority = new Authority();
        authority.id = uuid.v1();
        authority.emergencyVehicles = [];
        authority.name = 'Government Tartu';
        authority.user = await em.save(user);

        const ambulance = new EmergencyVehicle();
        ambulance.id = uuid.v1();
        ambulance.name = 'Laura';
        ambulance.type = EmergencyVehicleType.Ambulance;

        await em.save(ambulance);

        const police = new EmergencyVehicle();
        police.id = uuid.v1();
        police.name = 'Madis';
        police.type = EmergencyVehicleType.Police;

        await em.save(police);

        const fireTruck = new EmergencyVehicle();
        fireTruck.id = uuid.v1();
        fireTruck.name = 'Aquaman';
        fireTruck.type = EmergencyVehicleType.FireTruck;

        await em.save(fireTruck);

        authority.emergencyVehicles = [ambulance, police, fireTruck];

        return await em.save(authority);
    }
}
