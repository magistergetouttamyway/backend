import { Connection } from 'typeorm';
import { Factory, Seed } from 'typeorm-seeding';
import * as uuid from 'uuid';
import { Client } from '../../api/models/Client';
import { User } from '../../api/models/User';

export class CreateBruce implements Seed {
    public async seed(factory: Factory, connection: Connection): Promise<User> {
        const em = connection.createEntityManager();

        const client = new Client();
        client.id = uuid.v1();
        client.firstName = 'Bruce';
        client.lastName = 'Wayne';

        const user = new User();
        user.id = uuid.v1();
        user.email = 'bruce.wayne@wayne-enterprises.com';
        user.username = 'bruce';
        user.password = '1234';
        user.client = await em.save(client);

        return await em.save(user);
    }
}
