import { MicroframeworkLoader, MicroframeworkSettings } from 'microframework-w3tec';
import { env } from '../env';
import { useSocketServer } from 'socket-controllers';

export const socketIOLoader: MicroframeworkLoader = (settings: MicroframeworkSettings | undefined) => {
    if (settings) {
        const io = require('socket.io')(settings.getData('express_server'));

        useSocketServer(io, {
            controllers: env.app.dirs.socketControllers,
            middlewares: env.app.dirs.socketMiddlewares,
        });
    }
};
